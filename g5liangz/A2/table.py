if __name__ == '__main__':
    with open('table.txt', 'r') as ip:
	all = ip.readlines()
	with open('table2.txt', 'w') as op:
	    op.write('File, Memory size, Hit count, Miss count, Clean Evictions, Total Ref, Hit Rate, Miss Rate\n')
	    count = 1
	    op.write('Simpleloop   ')
	    op.write('rand  ')
	    for line in all:
		words = line.split(' ')
		try:
		    temp = float(words[-1])
		    op.write(words[-1].strip() + ' ')
		    if line[:9] == 'Miss rate':
			op.write('\n') 
			if count // 20 == 0:
			    op.write('Simpleloop   ')
			elif count // 20 == 1:
			    op.write('Blocked      ')
			elif count // 20 == 2:
			    op.write('Matmul       ')
			elif count // 20 == 3:
			    op.write('Penguin      ')
			
			if (count // 4) % 5 == 0:
			    op.write('rand  ')
			elif (count // 4) % 5 == 1:
			    op.write('fifo  ')
			elif (count // 4) % 5 == 2:
			    op.write('lru   ')
			elif (count // 4) % 5 == 3:
			    op.write('clock ')
			elif (count // 4) % 5 == 4:
			    op.write('opt   ')
			count += 1
		except Exception:
		    pass