#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"
#include "sim.h"

extern int debug;

extern struct frame *coremap;

extern char *tracefile;

int *last_index; //An array with memsize size. Its used to record the next
                //appear index in the future. Store -1 if no futrue ref.

tree *root; //base tree store all vaddr as key, the head of linklist is
            //also store here

void link_list_insert(link_list *current, int idx);
link_list *tree_find(tree *root, addr_t vaddr);
void tree_insert(tree *root, addr_t vaddr, int idx);
/* Page to evict is chosen using the optimal (aka MIN) algorithm. 
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int opt_evict() {
	int i;
    int greatest = last_index[0];
    int greatest_idx = 0;
    for(i = 0; i<memsize;i++){
        if(last_index[i] == -1){
            return i;
        }
        if(greatest < last_index[i]){
            greatest = last_index[i];
            greatest_idx = i;
        }
    }
	return greatest_idx;
}

/* This function is called on each access to a page to update any information
 * needed by the opt algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void opt_ref(pgtbl_entry_t *p) {
    if(!p->idx){
        p->idx = tree_find(root, p->vaddr);
    }
    if(p->idx->next){
        p->idx = p->idx->next;
        last_index[p->frame >> PAGE_SHIFT] = p->idx->index;
    }
    else{
        last_index[p->frame >> PAGE_SHIFT] = -1;
    }
	return;
}

/* Initializes any data structures needed for this
 * replacement algorithm.
 */
void opt_init() {
    FILE *tfp = stdin;
    if((tfp = fopen(tracefile, "r")) == NULL) {
        perror("Error opening tracefile from opt.c");
        exit(1);
    }
    char buf[MAXLINE];
    addr_t vaddr = 0;
    char type;
    int idx = 0;
    last_index = malloc(sizeof(int)*memsize);
    while(fgets(buf, MAXLINE, tfp) != NULL) {
        if(buf[0] != '=') {
            sscanf(buf, "%c %lx", &type, &vaddr);
            if(debug)  {
                printf("%c %lx\n", type, vaddr);
            }
            if(!root){
                root = malloc(sizeof(struct tree));
                root->vaddr = vaddr;
                link_list *new = malloc(sizeof(struct link_list));
                new->index = idx;
                root->index = new;
                root->last = new;
            }
            else{
                tree_insert(root, vaddr, idx);
            }
            idx ++;
        } else {
            continue;
        }

    }
}
void tree_insert(tree *root, addr_t vaddr, int idx){
    //getchar();
    //printf("Vaddr: %lx, Root: %lx, indx: %d\n", vaddr, root->vaddr, idx);
    if(vaddr == root->vaddr){
        //printf("found\n");
        link_list_insert(root->last, idx);
        root->last = root->last->next;
    }
    else if(vaddr < root->vaddr){
        //printf("left\n");
        if(!root->left){
            tree *new_t = malloc(sizeof(struct tree));
            root->left = new_t;
            new_t->vaddr = vaddr;
            link_list *new_l = malloc(sizeof(struct link_list));
            new_l->index = idx;
            new_t->index = new_l;
            new_t->last = new_l;
        }
        else{
            tree_insert(root->left, vaddr, idx);
        }
    }
    else{
        //printf("right\n");
        if(!(root->right)){
            //printf("new");
            tree *new_t = malloc(sizeof(struct tree));
            root->right = new_t;
            new_t->vaddr = vaddr;
            link_list *new_l = malloc(sizeof(struct link_list));
            new_l->index = idx;
            new_t->index = new_l;
            new_t->last = new_l;
        }
        else{
            //printf("recur\n");
            tree_insert(root->right, vaddr, idx);
        }
    }
}
link_list *tree_find(tree *root, addr_t vaddr){
    //printf("vaddr %lu,root->vaddr %lu\n", vaddr, root->vaddr);
    //getchar();
    if(vaddr == root->vaddr){
        //printf("1\n");
        return root->index;
    }
    else if(vaddr < root->vaddr){
        //printf("2\n");
        return tree_find(root->left, vaddr);
    }
    else{
        //printf("%lu\n", root->right->vaddr);
        return tree_find(root->right, vaddr);
    }
}

void link_list_insert(link_list *current, int idx){
    if(!current->next){
        link_list *new = malloc(sizeof(link_list));
        current->next = new;
        new->index = idx;
    }
    else{
        link_list_insert(current->next, idx);
    }
}
