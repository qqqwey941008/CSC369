#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

int *idxA = NULL; 

/* Page to evict is chosen using the accurate LRU algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */

int lru_evict() {
    int space = -1;
    int i;
	for(i=0;i<memsize;i++){
        if(idxA[i] == (memsize - 1)){
            space = i;
            idxA[i] = 0;
        }
        else{
            idxA[i] ++;
        }
    }
	return space;
}

/* This function is called on each access to a page to update any information
 * needed by the lru algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void lru_ref(pgtbl_entry_t *p) {
    unsigned int f = p->frame >> PAGE_SHIFT;
    int temp = idxA[f];
    int i;
    for(i=0;i<memsize;i++){
        if(idxA[i] < temp){
            idxA[i] ++;
        }
    }
    idxA[f] = 0;
	return;
}


/* Initialize any data structures needed for this 
 * replacement algorithm 
 */
void lru_init() {
    idxA = malloc(sizeof(int) * memsize);
    int i;
    for(i=0;i<memsize;i++){
        idxA[i] = memsize - i - 1;
    }
}
