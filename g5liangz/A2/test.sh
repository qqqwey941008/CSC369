#!/bin/bash
for i in rand fifo lru clock opt
do
    echo "testing file simpleloop with $i"
    echo "============================"
    for j in 50 100 150 200
    do 
        echo $j
        echo "-----------------------------"
        ./sim -f traceprogs/tr-simpleloop.ref -m $j -s 3000 -a $i | tail -n 7
        echo "-----------------------------\n"
    done
    echo "============================="
    echo ""
done

for i in rand fifo lru clock opt
do
    echo "testing file blocked with $i"
    echo "============================"
    for j in 50 100 150 200
    do 
        echo $j
        echo "-----------------------------"
        ./sim -f traceprogs/tr-blocked.ref -m $j -s 3000 -a $i | tail -n 7
        echo "-----------------------------\n"
    done
    echo "============================="
    echo ""
done

for i in rand fifo lru clock opt
do
    echo "testing file matmul with $i"
    echo "============================"
    for j in 50 100 150 200
    do 
        echo $j
        echo "-----------------------------"
        ./sim -f traceprogs/tr-matmul.ref -m $j -s 3000 -a $i | tail -n 7
        echo "-----------------------------\n"
    done
    echo "============================="
    echo ""
done

for i in rand fifo lru clock opt
do
    echo "testing file penguin with $i"
    echo "============================"
    for j in 50 100 150 200
    do 
        echo $j
        echo "-----------------------------"
        ./sim -f traceprogs/tr-penguin.ref -m $j -s 4000 -a $i | tail -n 7
        echo "-----------------------------\n"
    done
    echo "============================="
    echo ""
done