#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"

unsigned char *disk;


int main(int argc, char **argv) {

    if(argc != 2) {
        fprintf(stderr, "Usage: readimg <image file name>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
	perror("mmap");
	exit(1);
    }

    struct ext2_super_block *sb = (struct ext2_super_block *)(disk + 1024);
    struct ext2_group_desc *bg = (struct ext2_group_desc *)(disk + 2048);
    struct ext2_inode *inode_table = (struct ext2_inode *)(disk + 5120);

    printf("Inodes: %d\n", sb->s_inodes_count);
    printf("Blocks: %d\n", sb->s_blocks_count);
    printf("Block group:\n");
    printf("    block bitmap: %d\n", bg->bg_block_bitmap);
    printf("    inode bitmap: %d\n", bg->bg_inode_bitmap);
    printf("    inode table: %d\n", bg->bg_inode_table);
    printf("    free blocks: %d\n", bg->bg_free_blocks_count);
    printf("    free inodes: %d\n", bg->bg_free_inodes_count);
    printf("    used_dirs: %d\n", bg->bg_used_dirs_count);
    
    int i, j;
    printf("Block bitmap: ");
    char *block_bitmap = (char *)(disk + 1024 * bg->bg_block_bitmap);
    char *inode_bitmap = (char *)(disk + 1024 * bg->bg_inode_bitmap);
    for(i = 0; i<sb->s_blocks_count / 8;i++){
        for(j = 0; j < 8; j++){
            if(block_bitmap[i] & (1<<j)){
                printf("1");
            }
            else{
                printf("0");
            }
        }
        printf(" ");
    }
    printf("\nInode bitmap: ");
    for(i = 0; i<sb->s_inodes_count / 8;i++){
        for(j = 0; j < 8; j++){
            if(inode_bitmap[i] & (1<<j)){
                printf("1");
            }
            else{
                printf("0");
            }
        }
        printf(" ");
    }

    char type = 'e';


    printf("\n\nInodes:\n");
    for(i = 0; i<sb->s_inodes_count;i++){
        if(inode_bitmap[(int)(i/8)] & (1<<(i%8))){
            if(i == 1 || i > 10){
                    if(inode_table[i].i_mode & EXT2_S_IFLNK){
                        type = 'f';
                    }
                    else if(inode_table[i].i_mode & EXT2_S_IFREG){
                        type = 'f';
                    }
                    else if(inode_table[i].i_mode & EXT2_S_IFDIR){
                        type = 'd';
                    }   
                printf("[%d] type: %c size: %d links: %d blocks: %d\n", i+1, type, inode_table[i].i_size, inode_table[i].i_links_count, inode_table[i].i_blocks);
                printf("[%d] Block: ", i+1);
                for(j = 0; j < 15; j ++){
                    if(inode_table[i].i_block[j]){
                        printf("%d ", inode_table[i].i_block[j]);
                    }
                }
                printf("\n");
                
            }
        }
    }

    return 0;
}
