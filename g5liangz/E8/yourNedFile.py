import sys

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')
    try:
        input_file = sys.argv[1]
        output_file =sys.argv[2]
    except:
        print('usage python table <input_file> <output_file>')
    else:
        instruction = {}
        data = {}
        with open(input_file, 'r') as input_f:
            with open(output_file, 'w') as output_f:
                for each in input_f.readline():
		    temp = each.split(',')
		    if temp[1] == 'I':
			if not temp[0][:-3] in instruction:
			    instruction[temp[0][:-3]] = 1
			else:
			    instruction[temp[0][:-3]] += 1
		    else:
			if not temp[0][:-3] in data:
			    data[temp[0][:-3]] = 1
			else:
			    data[temp[0][:-3]] += 1
                output_f.write('Instructions\n')
                for key in instruction:
		    output_f.write(key+',')
		    output_f.write(instruction[key]+'\n')
                output_f.write('\nData\n')
                for key in data:
		    output_f.write(key+',')
		    output_f.write(data[key]+'\n')

