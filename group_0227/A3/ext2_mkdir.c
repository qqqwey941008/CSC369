#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
#include "util.c"
#include <time.h>
unsigned char *disk;


int main(int argc, char **argv) {
    if(argc != 3) {
        fprintf(stderr, "Usage: ext2_cp <image file name> <source_path> <dest_path>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
    perror("mmap");
    exit(1);
    }

    struct ext2_super_block *sb;
    struct ext2_group_desc *bg;
    char *block_bitmap;
    char *inode_bitmap;
    struct ext2_inode *inode_table;
    find_struct(disk, &sb, &bg, &inode_table, &block_bitmap, &inode_bitmap);
    if (find_inode(disk, argv[2], NULL,0)){
        fprintf(stderr,"File already exist\n");
        return EEXIST;
    }
    //Parent:
    time_t cur_time = time(NULL);
    char * filename;
    char * path = split_last_path(argv[2],&filename);
    struct ext2_inode * parent_dir = NULL;
    int parent_inode_num;
    //IF is /
    if (strlen(path) == 0){
        parent_dir = &inode_table[EXT2_ROOT_INO-1];
        parent_inode_num = EXT2_ROOT_INO;
    }
    else{ // If is /some/some..
        parent_dir = find_inode_and_inode_num(disk, path, NULL, &parent_inode_num,0);
    }
    //If parent's dir not found
    if (!parent_dir){
        fprintf(stderr,"No such file or directory\n");
        return ENOENT;
    }
    if (!(parent_dir->i_mode & EXT2_S_IFDIR)){
        fprintf(stderr, "Not a directory\n");
        return EEXIST;
    }

    //TODO: might minus more
    bg->bg_used_dirs_count++;

    struct ext2_dir_entry_2* new_entry = NULL;
    //The entry length
    int dir_len = first_divided_by_4(sizeof(struct ext2_dir_entry) + strlen(filename));
    int return_len = 0;
    new_entry = find_first_empty_entry_in_dir(disk, block_bitmap, parent_dir, dir_len, &return_len);
    new_entry->rec_len = return_len;
    new_entry->name_len = strlen(filename);
    new_entry->file_type = EXT2_FT_DIR;
    strncpy(new_entry->name, filename, strlen(filename));
    //Get empty inode num
    int empty_inode_num = find_first_empty_inode(disk);
    struct ext2_inode * empty_inode = &inode_table[empty_inode_num];
    //SET inode bit map
    setBit(inode_bitmap, empty_inode_num);
    //Init empty inode
    memset(empty_inode, 0 , sizeof(struct ext2_inode));
    empty_inode->i_mode = EXT2_S_IFDIR;
    empty_inode->i_size = EXT2_BLOCK_SIZE;
    empty_inode->i_atime = cur_time;
    empty_inode->i_ctime = cur_time;
    empty_inode->i_mtime = cur_time;
    empty_inode->i_links_count = 2;
    empty_inode->i_blocks = 2;
    //Get empty block num
    int empty_block_num = find_first_empty_block(disk);
    //Set block bit map
    setBit(block_bitmap, empty_block_num);
    char * empty_block = (char *)(disk + EXT2_BLOCK_SIZE * (empty_block_num+1));
    empty_inode->i_block[0] = (empty_block_num+1);
    //Dot
    struct ext2_dir_entry_2 * dot = (struct ext2_dir_entry_2 * )empty_block;
    dot->inode = empty_inode_num + 1;
    dot->name_len = strlen(".");
    dot->file_type = EXT2_FT_DIR;
    dot->name[0] = '.';
    dot->rec_len = first_divided_by_4(sizeof(struct ext2_dir_entry) + dot->name_len);
    printf("len: %d\n", sizeof(struct ext2_dir_entry));
    //DotDot
    struct ext2_dir_entry_2 * dotdot = (struct ext2_dir_entry_2 * )(empty_block + dot->rec_len);
    dotdot->inode = parent_inode_num;
    dotdot->name_len = strlen("..");
    dotdot->file_type = EXT2_FT_DIR;
    dotdot->name[0] = '.';
    dotdot->name[1] = '.';
    dotdot->rec_len = EXT2_BLOCK_SIZE - dot->rec_len;

    new_entry->inode = empty_inode_num + 1;

    parent_dir->i_links_count++;
    return 0;

}