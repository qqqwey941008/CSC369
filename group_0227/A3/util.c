#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"
#include <string.h>

#define MAXSYMLOOP 10
#define SINGLE_INDIRECT_BLOCK_NUM 12

int getBit(char* map, int pos);
struct ext2_inode * find_inode(unsigned char *disk, char * path, char ** filename, int reclevel);
void find_struct(unsigned char *disk, struct ext2_super_block ** sb, struct ext2_group_desc ** gd, struct ext2_inode ** inodetable, char ** blockmap, char ** inodemap);
unsigned int find_index_in_dir(unsigned char * disk,char * blockmap, struct ext2_inode * inodetable,char *filename);
unsigned int find_first_empty_inode(unsigned char *disk);
unsigned int find_first_empty_block(unsigned char *disk);

char * redirect_symbolic_link_node(unsigned char * disk ,struct ext2_inode * s_node);

//IF FILENAME = NULL Then do nothing about that, if it has value, it points to the last path. reclevel is the recursion level of this function. 
//reclevel need to be less than MAXSYMLOOP in case that the self link symbolic link happen.
//
struct ext2_inode * find_inode(unsigned char *disk, char * path, char ** filename,  int reclevel){
    if (reclevel > MAXSYMLOOP){
        fprintf(stderr, "Too many levels of symbolic links\n");
        exit(ENOENT);
    }
    char * copy = malloc(sizeof(char) * (strlen(path)+1));
    strncpy(copy, path, strlen(path));
    copy[strlen(path)] = '\0';

    char *path_token = strtok(copy, "/");
    struct ext2_group_desc *gd;
    struct ext2_inode * inodetable;
    struct ext2_inode * cur;
    char * blockmap;
    unsigned int ididx = 0;
    find_struct(disk, NULL, &gd, &inodetable, &blockmap, NULL);
    cur = &inodetable[EXT2_ROOT_INO-1];

    while(path_token){
    	printf("Current path: %s\n", path_token);
        ididx = find_index_in_dir(disk, blockmap, cur,path_token);
        if(ididx){
        	cur = &inodetable[ididx-1];
            if(filename){
                *filename = path_token;
            }
            if(!(cur->i_mode ^ EXT2_S_IFLNK)){
                char * redirected_path = redirect_symbolic_link_node(disk, cur);
                cur = find_inode(disk, redirected_path, filename, reclevel+1);
                free(redirected_path);
            }
        }
        else{
        	return NULL;
        }
        path_token = strtok(NULL, "/");
    }
    if (!filename){
        free(copy);
    }
    return cur;
}
//This is baiscally the same as find_inode_and_inode_num except that it won't translate any symlink during the process. Useful for ln
struct ext2_inode * find_inode_ignore_symlink(unsigned char *disk, char * path, char ** filename, int * inode_num){
    char * copy = malloc(sizeof(char) * (strlen(path)+1));
    strncpy(copy, path, strlen(path));
    copy[strlen(path)] = '\0';

    char *path_token = strtok(copy, "/");
    struct ext2_group_desc *gd;
    struct ext2_inode * inodetable;
    struct ext2_inode * cur;
    char * blockmap;
    unsigned int ididx = 0;
    find_struct(disk, NULL, &gd, &inodetable, &blockmap, NULL);
    cur = &inodetable[EXT2_ROOT_INO-1];

    while(path_token){
        printf("Current path: %s\n", path_token);
        ididx = find_index_in_dir(disk, blockmap, cur,path_token);
        if(ididx){
            cur = &inodetable[ididx-1];
            *inode_num = ididx;
            if(filename){
                *filename = path_token;
            }
        }
        else{
            return NULL;
        }
        path_token = strtok(NULL, "/");
    }
    if (!filename){
        free(copy);
    }
    return cur;
}

//IF FILENAME = NULL Then do nothing about that, if it has value, it points to the last path 
//inode_num is the inode number.
struct ext2_inode * find_inode_and_inode_num(unsigned char *disk, char * path, char ** filename, int * inode_num, int reclevel){
    if (reclevel > MAXSYMLOOP){
        fprintf(stderr, "Too many levels of symbolic links\n");
        exit(ENOENT);
    }
    char * copy = malloc(sizeof(char) * (strlen(path)+1));
    strncpy(copy, path, strlen(path));
    copy[strlen(path)] = '\0';

    char *path_token = strtok(copy, "/");
    struct ext2_group_desc *gd;
    struct ext2_inode * inodetable;
    struct ext2_inode * cur;
    char * blockmap;
    unsigned int ididx = 0;
    find_struct(disk, NULL, &gd, &inodetable, &blockmap, NULL);
    cur = &inodetable[EXT2_ROOT_INO-1];

    while(path_token){
        // printf("Current path: %s\n", path_token);
        ididx = find_index_in_dir(disk, blockmap, cur,path_token);
        if(ididx){
            cur = &inodetable[ididx-1];
            *inode_num = ididx;
            if(filename){
                *filename = path_token;
            }
            if(!(cur->i_mode ^ EXT2_S_IFLNK)){
                char * redirected_path = redirect_symbolic_link_node(disk, cur);
                cur = find_inode_and_inode_num(disk, redirected_path, filename, inode_num ,reclevel+1);
                free(redirected_path);
            }
        }
        else{
            return NULL;
        }
        path_token = strtok(NULL, "/");
    }
    if (!filename){
        free(copy);
    }
    return cur;
}



///  /a/b/c -> /a/b c , /a THIS will modify the original path!
char * split_last_path(char * path, char** filename){
    int len = strlen(path);
    int i =len-1;
    for(;i>=0;i--){
        if ((path[i] == '/') && (i == len-1)){
            path[i] = '\0';
            continue;
        }
        else if (path[i] == '/') {
            path[i] = '\0';
            *filename = &path[i+1];
            printf("%s\n", &path[i+1]);
            return path;
        }
    }
    return NULL;
}

int getBit(char* map, int pos){
    int new_pos = pos/8;
    int offset = pos%8;
    if (pos<0){
        return 0;
    }
    return map[new_pos] & 1 << offset;
}

void setBit(char * map, int pos){
	int new_pos = pos/8;
	int offset = pos%8;
	map[new_pos] = map[new_pos] | 1 << offset;
}

void clearBit(char * map, int pos){
	int new_pos = pos/8;
	int offset = pos%8;
	map[new_pos] = map[new_pos] & ~(1 << offset);
}

int first_divided_by_4(int start){
    int i = start;
    while (i%4!=0){
        i++;
    }
    return i;
}


void find_struct(unsigned char *disk, struct ext2_super_block ** sb, struct ext2_group_desc ** gd, struct ext2_inode ** inodetable, char ** blockmap, char ** inodemap){
	*gd = (struct ext2_group_desc * )(disk + 2 * EXT2_BLOCK_SIZE);
	if(sb){
		*sb = (struct ext2_super_block *)(disk + EXT2_BLOCK_SIZE);
	}
	if(inodetable){
		*inodetable = (struct ext2_inode *)(disk + (*gd)->bg_inode_table * EXT2_BLOCK_SIZE);
	}
	if(blockmap){
		*blockmap = (char *)(disk + (*gd)->bg_block_bitmap * EXT2_BLOCK_SIZE);
	}
	if(inodemap){
		*inodemap = (char *)(disk + (*gd)->bg_inode_bitmap * EXT2_BLOCK_SIZE);
	}

}
//Return index +1; Inode number
unsigned int find_index_in_dir(unsigned char * disk,char * blockmap, struct ext2_inode * inodetable,char *filename){
	int i = 0;
	int cur_len = 0;
	int count = 0;
	struct ext2_dir_entry_2* d_p = NULL;
	unsigned int* indirect_block = NULL;
	int offset = 0;
	//DIRECT NODE
    if (getBit(blockmap, inodetable->i_block[SINGLE_INDIRECT_BLOCK_NUM]-1)){
        count = 1;
    }
	while(count < (inodetable->i_blocks/2)){
		if (i< 12){
			d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]));
			cur_len = 0;
			if (getBit(blockmap,inodetable->i_block[i]-1)){
				count++;
				while(cur_len < EXT2_BLOCK_SIZE){
					if (strlen(filename) == d_p->name_len){
						if (!strncmp(filename, d_p->name,d_p->name_len)){
							return d_p->inode;
						}
					}
					cur_len += d_p->rec_len;
					d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]) + cur_len);
				}
			}
		} 
		else{
			indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[SINGLE_INDIRECT_BLOCK_NUM]));
			offset = i - 12;
			d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[offset]);
			cur_len = 0;
			if (getBit(blockmap, indirect_block[offset]-1)){
				count++;
				while(cur_len < EXT2_BLOCK_SIZE){
					if (strlen(filename) == d_p->name_len){
						if (!strncmp(filename, d_p->name,d_p->name_len)){
							return d_p->inode;
						}
					}
					cur_len += d_p->rec_len;
					d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[offset]) + cur_len);
				}
			}
		}
		i++;
	}
	return 0;
}
//Return the index not inode number. inode num = index + 1 
unsigned int find_first_empty_inode(unsigned char *disk){
	int i = EXT2_GOOD_OLD_FIRST_INO;
	struct ext2_super_block *sb;
	struct ext2_group_desc *gd;
    struct ext2_inode * inodetable;
    char * inodemap;
    find_struct(disk, &sb, &gd, &inodetable, NULL, &inodemap);
	for(;i<sb->s_inodes_count;i++){
		if (!getBit(inodemap, i)){
            gd->bg_free_inodes_count--;
            sb->s_free_inodes_count--;
			return i;
		}
	}
	return 0;
}
//Return the index not block number. block num = index + 1 
unsigned int find_first_empty_block(unsigned char * disk){
	int i = 0;
	struct ext2_super_block *sb;
	struct ext2_group_desc *gd;
    char * blockmap;
    find_struct(disk, &sb, &gd, NULL, &blockmap, NULL);
    for(;i<sb->s_blocks_count;i++){
    	if(!getBit(blockmap,i)){
            gd->bg_free_blocks_count--;
            sb->s_free_blocks_count--;
    		return i;
    	}
    }
    return 0;
}

struct ext2_dir_entry_2* find_first_empty_entry_in_dir(unsigned char * disk,char * blockmap, struct ext2_inode * inodetable, int entry_len, int *return_len){
    int i = 0;
    int cur_len = 0;
    int count = 0;
    struct ext2_dir_entry_2* d_p = NULL;
    unsigned int* indirect_block = NULL;
    int offset = 0;
    //DIRECT NODE
    if (getBit(blockmap, inodetable->i_block[SINGLE_INDIRECT_BLOCK_NUM]-1)){
        count = 1;
    }
    while(count < (inodetable->i_blocks/2)){
        if (i< 12){
            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]));
            cur_len = 0;
            if (getBit(blockmap,inodetable->i_block[i]-1)){
                count++;
                while(cur_len < EXT2_BLOCK_SIZE){
                    printf("(%d %d)\n",d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len), first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len));
                    * return_len = (d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len));
                    if ((d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len)) >= entry_len){
                        d_p->rec_len = first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len);
                        return (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]) + cur_len + d_p->rec_len);
                    }
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]) + cur_len);
                }
            }
        } 
        else{ 
            // TODO: FIX THIS!
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[SINGLE_INDIRECT_BLOCK_NUM]));
            offset = i - 12;
            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[offset]);
            cur_len = 0;
            if (getBit(blockmap, indirect_block[offset]-1)){
                count++;
                while(cur_len < EXT2_BLOCK_SIZE){
                    * return_len = (d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len));
                    if ((d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len)) >= entry_len){
                        d_p->rec_len = first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len);
                        return  (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[offset]) + cur_len + d_p->rec_len);
                    }
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[offset]) + cur_len);
                }
            }
        }
        i++;
    }
    //IF no block currently exist can be added in add into new block;
    i = 0;
    while (i < 15){
        if (i<12){
            if (!getBit(blockmap,inodetable->i_block[i]-1)){
                inodetable->i_blocks += 2;
                inodetable->i_size += EXT2_BLOCK_SIZE;
                int block_num = find_first_empty_block(disk);
                setBit(blockmap, block_num);
                inodetable->i_block[i] = block_num + 1;
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]));
                *return_len = EXT2_BLOCK_SIZE;
                return d_p;
            }
        }
        else if (i == SINGLE_INDIRECT_BLOCK_NUM){ //TODO : FIX THIS!
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[SINGLE_INDIRECT_BLOCK_NUM]));
            int j = 0;
            while(j < EXT2_BLOCK_SIZE/sizeof(unsigned int *)){
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[j]);
                cur_len = 0;
                if (!(getBit(blockmap, indirect_block[j]-1))){
                    while(cur_len < EXT2_BLOCK_SIZE){
                        * return_len = (d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len));
                        if ((d_p->rec_len - first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len)) >= entry_len){
                            d_p->rec_len = first_divided_by_4(sizeof(struct ext2_dir_entry_2) + d_p->name_len);
                            return  (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[j]) + cur_len + d_p->rec_len);
                        }
                        cur_len += d_p->rec_len;
                        d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[j]) + cur_len);
                    }
                }
            }
            break;
        }
        i++;
    }
    return NULL;
}



// struct ext2_dir_entry_2* find_dp(unsigned char * disk, char * path){
// 	char *path_token = strtok(path, "/");
//     struct ext2_group_desc *gd;
//     struct ext2_inode * inodetable;
//     struct ext2_inode * cur;
//     char * blockmap;
//     unsigned int ididx = 0;
//     find_struct(disk, NULL, &gd, &inodetable, &blockmap, NULL);
//     cur = &inodetable[EXT2_ROOT_INO-1];
//     struct ext2_inode *second_last;
//     char *last_token;


//     while(path_token){
//     	// printf("Current path: %s\n", path_token);
//         ididx = find_index_in_dir(disk, blockmap, cur, path_token);
//         if(ididx){
//     		second_last = cur;
//         	cur = &inodetable[ididx-1];
//         }
//         else{
//         	return NULL;
//         }
//         last_token = path_token;
//         path_token = strtok(NULL, "/");
//     }

//     int i = 0;
// 	int cur_len = 0;
// 	int count = 0;
// 	struct ext2_dir_entry_2* d_p = NULL;
// 	unsigned int* indirect_block = NULL;
// 	int offset = 0;
//     while(count < (second_last->i_blocks/2)){
// 		if (i< 12){
// 			d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (second_last->i_block[i]));
// 			cur_len = 0;
// 			if (getBit(blockmap,second_last->i_block[i]-1)){
// 				count++;
// 				while(cur_len < EXT2_BLOCK_SIZE){
// 					if (strlen(last_token) == d_p->name_len){
// 						if (!strncmp(last_token, d_p->name,d_p->name_len)){
// 							return d_p;
// 						}
// 					}
// 					cur_len += d_p->rec_len;
// 					d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (second_last->i_block[i]) + cur_len);
// 				}
// 			}
// 		} 
// 		else{
// 			indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * (second_last->i_block[SINGLE_INDIRECT_BLOCK_NUM]));
// 			offset = i - 12;
// 			d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[offset]);
// 			cur_len = 0;
// 			if (getBit(blockmap, indirect_block[offset]-1)){
// 				count++;
// 				while(cur_len < EXT2_BLOCK_SIZE){
// 					if (strlen(last_token) == d_p->name_len){
// 						if (!strncmp(last_token, d_p->name,d_p->name_len)){
// 							return d_p;
// 						}
// 					}
// 					cur_len += d_p->rec_len;
// 					d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (indirect_block[offset]) + cur_len);
// 				}
// 			}
// 		}
// 		i++;
// 	}

//     return NULL;
// }

char* last_token(char * path){
    char * copy = malloc(sizeof(char) * (strlen(path)+1));
    strncpy(copy, path, strlen(path));
    copy[strlen(path)] = '\0';


	char *path_token = strtok(copy, "/");
	char *last;
	while(path_token){
    	// printf("Current path: %s\n", path_token);
    	last = path_token;
        path_token = strtok(NULL, "/");
    }
    return last;
}

int exist_inode(unsigned char *disk, char * path, char ** filename, char * last){
    char * copy = malloc(sizeof(char) * (strlen(path)+1));
    strncpy(copy, path, strlen(path));
    copy[strlen(path)] = '\0';

    char *path_token = strtok(copy, "/");
    struct ext2_group_desc *gd;
    struct ext2_inode * inodetable;
    struct ext2_inode * cur;
    char * blockmap;
    unsigned int ididx = 0;
    find_struct(disk, NULL, &gd, &inodetable, &blockmap, NULL);
    cur = &inodetable[EXT2_ROOT_INO-1];

    while(path_token){
    	// printf("Current path: %s\n", path_token);
        ididx = find_index_in_dir(disk, blockmap, cur,path_token);
        if(ididx){
        	cur = &inodetable[ididx-1];
            if(filename){
                *filename = path_token;
            }
        }
        else{
        	return 1;
        }
        path_token = strtok(NULL, "/");
    }
    if(find_index_in_dir(disk, blockmap, cur, last)){
    	return 1;
    }
    return 0;
}

//Helper function to resolve one path, need to free the return string.
char * redirect_symbolic_link(unsigned char * disk ,char * path){
    struct ext2_inode * s_node = find_inode(disk, path, NULL,0);
    char * new_path = malloc((s_node->i_size+1) * sizeof(char) );
    if (s_node->i_size <= 60){
        memcpy(new_path, s_node->i_block, s_node->i_size);
        new_path[s_node->i_size] = '\0';
        return new_path;
    }
    else{
        char * path_in_block = (char *)(disk + EXT2_BLOCK_SIZE * s_node->i_block[0]);
        memcpy(new_path, path_in_block, s_node->i_size);
        new_path[s_node->i_size] = '\0';
        return new_path;
    }
}
//Helper function to resolve one path, need to free the return string.
//Given a node rather than link.
char * redirect_symbolic_link_node(unsigned char * disk ,struct ext2_inode * s_node){
    char * new_path = malloc((s_node->i_size+1) * sizeof(char) );
    if (s_node->i_size <= 60){
        memcpy(new_path, s_node->i_block, s_node->i_size);
        new_path[s_node->i_size] = '\0';
        return new_path;
    }
    else{
        char * path_in_block = (char *)(disk + EXT2_BLOCK_SIZE * s_node->i_block[0]);
        memcpy(new_path, path_in_block, s_node->i_size);
        new_path[s_node->i_size] = '\0';
        return new_path;
    }
}



//Function to resolve the symbolic path, return the resolved path, and set return_node if return_node != NULL
char * resolve_symbolic_link(unsigned char * disk, char * path, struct ext2_inode ** return_node){
    char * new_path = redirect_symbolic_link(disk, path);
    char * temp_path = new_path;
    struct ext2_inode * new_node = find_inode(disk, new_path, NULL,0);
    while (!(new_node->i_mode ^ EXT2_S_IFLNK)){
        new_path = redirect_symbolic_link(disk, new_path);
        free(temp_path);
        temp_path = new_path;
        new_node = find_inode(disk, new_path, NULL,0);
    }
    if (return_node){
        *return_node = new_node;
    }
    return new_path;
}


