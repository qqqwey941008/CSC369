#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
#include "util.c"


unsigned char *disk;


int main(int argc, char **argv) {
    int c;
    int symbol = 0;
    int fd;
    char * src;
    char * dst;
    while ((c = getopt (argc, argv, "s")) != -1){
        switch (c){
            case 's': symbol = 1;break;
            default:
                    fprintf(stderr, "Usage: ext2_ls <image file name> <-a> <path>\n");
                    exit(1);
                    break;
        }
    }

    if(argc != 4 && argc != 5) {
        fprintf(stderr, "Usage: ext2_ln <image file name> <source_path> <dest_path>\n");
        exit(1);
    }
    if (symbol){
        fd  = open(argv[2], O_RDWR);
        src = argv[3];
        dst = argv[4];
    }
    else{
        fd  = open(argv[1], O_RDWR);
        src = argv[2];
        dst = argv[3];
    }
    

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }

    struct ext2_super_block *sb;
    struct ext2_group_desc *bg;
    char *block_bitmap;
    char *inode_bitmap;
    struct ext2_inode *inode_table;
    find_struct(disk, &sb, &bg, &inode_table, &block_bitmap, &inode_bitmap);

    //Source: CHECK if exist
    int source_inode_num;
    struct ext2_inode * source_inode = find_inode_ignore_symlink(disk, src, NULL, &source_inode_num);
    if (!source_inode){
        fprintf(stderr,"No such file or directory\n");
        return ENOENT;
    }
    if (source_inode->i_mode & EXT2_S_IFDIR && source_inode->i_mode ^ EXT2_S_IFLNK){
        printf("%d\n", source_inode->i_mode);
        fprintf(stderr, "Not a file\n" );
        return EISDIR;
    }

    //Destination: CHECK Shouldnt exist
    if (find_inode(disk, dst, NULL,0)){
        fprintf(stderr,"File already exist\n");
        return EEXIST;
    }
    //Destination parent dir: check should exist
    char * filename = NULL;
    char * path = split_last_path(dst,&filename);
    if (!filename){
        fprintf(stderr,"No such file or directory\n");
        return ENOENT;  
    }
    struct ext2_inode * parent_dir_inode = NULL;
    if (strlen(path) == 0){
        parent_dir_inode = &inode_table[EXT2_ROOT_INO-1];
    }
    else{
        parent_dir_inode = find_inode(disk, path, NULL, 0);
    }
    if (!parent_dir_inode){
        fprintf(stderr,"No such file or directory\n");
        return ENOENT;
    }
    if (!(parent_dir_inode->i_mode & EXT2_S_IFDIR)){
        fprintf(stderr, "Not a directory\n" );
        return EEXIST;
    }

    //
    int return_len;
    int entry_len = sizeof(struct ext2_inode) + strlen(filename);
    struct ext2_dir_entry_2 * new_entry = find_first_empty_entry_in_dir(disk, block_bitmap, parent_dir_inode, entry_len, &return_len);
    if (!symbol){
        new_entry->inode = source_inode_num;
        new_entry->rec_len = return_len;
        new_entry->name_len = strlen(filename);
        new_entry->file_type = EXT2_FT_REG_FILE;
        strncpy(new_entry->name, filename, new_entry->name_len);

        source_inode->i_links_count++;
    }
    else{
        //Entry
        int new_inode_num = find_first_empty_inode(disk);
        setBit(inode_bitmap, new_inode_num);
        new_entry->inode = new_inode_num+1;
        new_entry->rec_len = return_len;
        new_entry->name_len = strlen(filename);
        new_entry->file_type = EXT2_FT_SYMLINK;
        strncpy(new_entry->name, filename, new_entry->name_len);
        //Inode
        struct ext2_inode * dst_inode = &inode_table[new_inode_num];
        memset(dst_inode, 0 , sizeof(struct ext2_inode));
        dst_inode->i_mode = EXT2_S_IFLNK;
        dst_inode->i_size = strlen(src);
        dst_inode->i_links_count = 1;

        if (dst_inode->i_size <= 60){
            dst_inode->i_blocks = 0;
            memcpy(&dst_inode->i_block[0], src, dst_inode->i_size);
        }
        else{
            dst_inode->i_blocks = 2;
            int new_block_num = find_first_empty_block(disk);
            setBit(block_bitmap, new_block_num);
            dst_inode->i_block[0] = new_block_num + 1;
            char * new_path = (char *)(disk + EXT2_BLOCK_SIZE * (new_block_num + 1));
            memcpy(new_path, src, dst_inode->i_size);
        }
    }
    return 0;

}

