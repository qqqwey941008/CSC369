#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ext2.h"

unsigned char *disk;
int getBit(char* map, int pos);
void print_inode(struct ext2_inode * inodetable, int i);
void print_directory(unsigned char * disk,struct ext2_inode * inodetable, int i);

int main(int argc, char **argv) {

    if(argc != 2) {
        fprintf(stderr, "Usage: readimg <image file name>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
	perror("mmap");
	exit(1);
    }

    struct ext2_super_block *sb = (struct ext2_super_block *)(disk + EXT2_BLOCK_SIZE);
    printf("Inodes: %d\n", sb->s_inodes_count);
    printf("Blocks: %d\n", sb->s_blocks_count);
    struct ext2_group_desc * gd = (struct ext2_group_desc * )(disk + 2 * EXT2_BLOCK_SIZE);
    printf("Block group:\n");
    printf("    block bitmap: %d\n",gd->bg_block_bitmap);
    printf("    inode bitmap: %d\n",gd->bg_inode_bitmap);
    printf("    inode table: %d\n",gd->bg_inode_table);
    printf("    free blocks: %d\n",gd->bg_free_blocks_count);
    printf("    free inodes: %d\n",gd->bg_free_inodes_count);
    printf("    used_dirs: %d\n",gd->bg_used_dirs_count);
    int i = 0;
    char * bitmap = (char *)(disk + gd->bg_block_bitmap * EXT2_BLOCK_SIZE);
    printf("Block bitmap: ");
    for (i=0 ; i < sb->s_blocks_count; i++){
        if (getBit(bitmap,i)){
            printf("1");
        }
        else{
            printf("0");
        }
        if ((i+1) % 8 == 0){
            printf(" ");
        }
    }
    printf("\n");
    char * inode_map = (char *)(disk + gd->bg_inode_bitmap * EXT2_BLOCK_SIZE);
    printf("Inode bitmap: ");
    for (i=0 ; i < sb->s_inodes_count; i++){
        if (getBit(inode_map,i)){
            printf("1");
        }
        else{
            printf("0");
        }
        if ((i+1) % 8 == 0){
            printf(" ");
        }
    }
    printf("\n");
    printf("\n");
    printf("Inodes:\n");
    struct ext2_inode * inodetable = (struct ext2_inode *)(disk + gd->bg_inode_table * EXT2_BLOCK_SIZE);
    print_inode(inodetable, 1);
    for (i=11; i< sb->s_inodes_count;i++){
        if(getBit(inode_map, i)){
            print_inode(inodetable, i);
        }
    }
    printf("\n");
    printf("Directory Blocks:\n");
    print_directory(disk,inodetable,1);
    for (i=11; i< sb->s_inodes_count; i++){
        if(getBit(inode_map,i)){
            if (inodetable[i].i_mode & EXT2_S_IFDIR){
                print_directory(disk,inodetable,i);
            }
        }
    }
    return 0;
}

int getBit(char* map, int pos){
    int new_pos = pos/8;
    int offset = pos%8;
    return map[new_pos] & 1 << offset;
}


void print_inode(struct ext2_inode * inodetable, int i){
    int j = 0;
    printf("[%d] type: ", i+1);
    if (inodetable[i].i_mode & EXT2_S_IFREG){
        if (inodetable[i].i_mode ^ EXT2_S_IFLNK){
            printf("f");
        }
        else{
            printf("s");  
        }
    }
    else if (inodetable[i].i_mode & EXT2_S_IFDIR){
        printf("d");
    }
    printf(" size: %d links: %d blocks: %d\n", inodetable[i].i_size, inodetable[i].i_links_count, inodetable[i].i_blocks);
    printf("[%d] Blocks:  ", i+1);
    for (j = 0; j<15; j++){
        if (j+1 > (inodetable[i].i_blocks /2)) break;
        else{
            printf("%d ", inodetable[i].i_block[j]);
        }
    }
    printf("\n");
}

void print_directory(unsigned char * disk,struct ext2_inode * inodetable, int i){
    int j = 0;
    int cur_len = 0;
    struct ext2_dir_entry_2* d_p = NULL;
    char type;
    for (j = 0;j<15 ;j++){
        if((j+1) > (inodetable[i].i_blocks /2)) break;
        else{
            cur_len = 0;
            printf("   DIR BLOCK NUM: %d (for inode %d)\n", inodetable[i].i_block[j],i+1);
            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * inodetable[i].i_block[j]);
            while(cur_len < EXT2_BLOCK_SIZE){
                if (d_p->file_type & EXT2_FT_REG_FILE){
                    if (d_p->file_type ^ EXT2_FT_SYMLINK){
                        type = 'f';
                    }
                    else{
                        type = 's';
                    }
                }
                else if (d_p->file_type & EXT2_FT_DIR){
                    type = 'd';
                }
                printf("Inode: %d rec_len: %d name_len: %d type= %c name=%.*s\n", d_p->inode, d_p->rec_len, d_p->name_len, type,d_p->name_len ,d_p->name);
                cur_len = cur_len + d_p->rec_len;
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * inodetable[i].i_block[j] + cur_len);
            }
        }
    }
}
