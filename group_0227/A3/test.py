#!/usr/bin/python
import os
import sys
import urllib2

url = "http://www.cdf.toronto.edu/~csc369h/winter/assignments/a3/images/"
imgname = ["emptydisk.img", "onefile.img", "deletedfile.img","onedirectory.img","hardlink.img","deleteddirectory.img","twolevel.img","largefile.img"]
cmd_list = {'ls':'./ext2_ls','cp':'./ext2_cp','rm':'./ext2_rm','ln':'./ext2_ln','mkdir':'./ext2_mkdir','read':'./readimage'}
def main():
    if len(sys.argv) != 2:
        print "Usage: test.py [img file]"
        return 
    filename = sys.argv[1]
    if filename in imgname:
        os.system("make all")
        if os.path.isfile(filename):
            os.system("rm -f " + filename)
            # pass
        data = urllib2.urlopen(url + filename)
        with open(filename,'w') as f:
            f.write(data.read())
            f.close()
        raw_cmd = raw_input('$ ')
        while raw_cmd != 'quit':
            try:
                cmd = raw_cmd.split(' ')
                if cmd[0] in cmd_list:
                    # print(raw_cmd.replace(cmd[0], cmd_list[cmd[0]]+' '+filename))
                    os.system(raw_cmd.replace(cmd[0], cmd_list[cmd[0]]+' '+filename))
                else:
                    print "Invalid command"
            except Exception as e:
                pass
            raw_cmd = raw_input('$ ')

    else:
        print "Invalid filename"
        return


if __name__ == '__main__':
    main()