#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
#include "util.c"

unsigned char *disk;

int main(int argc, char **argv) {
    if(argc != 4) {
        fprintf(stderr, "Usage: ext2_cp <image file name> <source_path> <dest_path>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
    perror("mmap");
    exit(1);
    }

    struct ext2_super_block *sb;
    struct ext2_group_desc *bg;
    char *block_bitmap;
    char *inode_bitmap;
    struct ext2_inode *inode_table;
    unsigned int* d_p = NULL;
    unsigned int* new_d_p = NULL;
    unsigned int* indirect_block = NULL;
    unsigned int* new_indirect_block = NULL;

    find_struct(disk, &sb, &bg, &inode_table, &block_bitmap, &inode_bitmap);


    struct ext2_inode *source;
    struct ext2_inode *dest;

    if(!(source = find_inode(disk, argv[2], NULL, 0))){
        printf("Source file not found\n");
        return ENOENT;
    }
    if(source->i_mode & EXT2_S_IFDIR){
        printf("Source must be a file\n");
        return ENOENT;
    }

    char *target_name; 
    char *dest_dir;

    if((dest = find_inode(disk, argv[3], NULL, 0))){
        if(!(dest->i_mode & EXT2_S_IFDIR)){
            printf("File already exits\n");
            return EEXIST;
        }
        else{
            target_name = last_token(argv[2]);
            dest_dir = malloc(sizeof(char) * (strlen(argv[3])+1));
            strncpy(dest_dir, argv[3], strlen(argv[3]));
            dest_dir[strlen(argv[3])] = '\0';
        }    
    }
    else{
        dest_dir = split_last_path(argv[3], &target_name);
        if(!dest_dir){
            dest_dir = "/";
            target_name = argv[3];
        }
        else if(strlen(dest_dir) == 0){
            dest_dir = "/";
        }
        if(!(dest = find_inode(disk, dest_dir, NULL, 0))){
            return ENOENT;
        }
    }
    
    if(exist_inode(disk, dest_dir, NULL, target_name)){
        printf("File already exits\n");
        return EEXIST;
    }

    unsigned int empty_block;
    unsigned int empty_inode = find_first_empty_inode(disk);
    struct ext2_inode *new_inode = &inode_table[empty_inode];
    printf("empty_inode: %d\n", empty_inode);
    //copy source's info to new_inode then reset new_node's iblock
    memset(new_inode, 0, sizeof(struct ext2_inode));
    new_inode->i_mode = source->i_mode;
    new_inode->i_size = source->i_size;
    new_inode->i_blocks = source->i_blocks;
    new_inode->i_links_count = 1;
    int i = 0;
    // for(i = 0; i < 15; i++){
    //     new_inode->i_block[i] = 0;
    // }
    // i = 0;


    //creating new inode and new blocks
    int count = 0;
    int indirect_block_is_set = 0;
    int bit = 0;
    if(getBit(block_bitmap, source->i_block[SINGLE_INDIRECT_BLOCK_NUM]-1)){
        bit ++;
    }

    while((count + bit) < (source->i_blocks/2)){
        if(i<SINGLE_INDIRECT_BLOCK_NUM){
            d_p = (unsigned int *)(disk + EXT2_BLOCK_SIZE * (source->i_block[i]));
            if (getBit(block_bitmap, source->i_block[i]-1)){
                empty_block = find_first_empty_block(disk);
                setBit(block_bitmap, empty_block);
                new_d_p = (unsigned int *)(disk + EXT2_BLOCK_SIZE * empty_block);
                memcpy(new_d_p, d_p, EXT2_BLOCK_SIZE);
                new_inode->i_block[count] = empty_block + 1;
                count ++;
            }
        }
        else{
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * source->i_block[SINGLE_INDIRECT_BLOCK_NUM]);
            d_p = (unsigned int *)(disk + EXT2_BLOCK_SIZE * indirect_block[i-SINGLE_INDIRECT_BLOCK_NUM]);
            if (getBit(block_bitmap, indirect_block[i-SINGLE_INDIRECT_BLOCK_NUM]-1)){
                if(count<SINGLE_INDIRECT_BLOCK_NUM){
                    empty_block = find_first_empty_block(disk);
                    new_d_p = (unsigned int *)(disk + EXT2_BLOCK_SIZE * empty_block);
                    memcpy(new_d_p, d_p, EXT2_BLOCK_SIZE);
                    setBit(block_bitmap, empty_block);
                    new_inode->i_block[count] = empty_block + 1;
                }
                else{
                    if(!indirect_block_is_set){
                        empty_block = find_first_empty_block(disk);
                        setBit(block_bitmap, empty_block);
                        new_inode->i_block[SINGLE_INDIRECT_BLOCK_NUM] = empty_block + 1;
                        indirect_block_is_set = 1;
                    }
                    empty_block = find_first_empty_block(disk);
                    setBit(block_bitmap, empty_block);
                    new_d_p = (unsigned int *)(disk + EXT2_BLOCK_SIZE * empty_block);
                    memcpy(new_d_p, d_p, EXT2_BLOCK_SIZE);
                    new_indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * new_inode->i_block[SINGLE_INDIRECT_BLOCK_NUM]);
                    new_indirect_block[count+bit-SINGLE_INDIRECT_BLOCK_NUM] = empty_block + 1;
                }
                count ++;
            }
        }
        i++;
    }

    setBit(inode_bitmap, empty_inode);

    int temp_rec_len;
    int needed_len = first_divided_by_4(strlen(target_name) + sizeof(struct ext2_dir_entry_2));
    new_d_p = find_first_empty_entry_in_dir(disk, block_bitmap, dest, needed_len, &temp_rec_len);
    new_d_p->name_len = strlen(target_name);
    strncpy(new_d_p->name, target_name, new_d_p->name_len);
    new_d_p->rec_len = temp_rec_len;
    new_d_p->inode = empty_inode + 1;
    printf("DONE\n");
    return 0;
}

    /*
    while(count < (dest->i_blocks/2)){
        if(i<12){
            d_p = (struct ext2_dir_entry_2 *)(disk + EXT2_BLOCK_SIZE * (dest->i_block[i]));
            cur_len = 0;
            if (getBit(block_bitmap,dest->i_block[i]-1)){
                while(cur_len < EXT2_BLOCK_SIZE){
                    if((d_p->name_len + sizeof(d_p)) != d_p->rec_len){//this dir_entry_p is the last one
                        int remain_size = d_p->rec_len - sizeof(d_p) - d_p->name_len;
                        if(remain_size > real_size){
                            d_p->rec_len = d_p->name_len + sizeof(d_p);
                            cur_len += d_p->rec_len;
                            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (dest->i_block[i]) + cur_len);
                            memcpy(d_p, source_dp, sizeof(struct ext2_dir_entry_2));
                            strncpy(d_p->name, source_dp->name, source_dp->name_len);
                            d_p->rec_len = EXT2_BLOCK_SIZE - cur_len;
                            printf("%d, asdaddsaadsa-%.*s\n",d_p->name_len, d_p->name_len ,d_p->name);
                            return 0;
                        }
                    }
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (dest->i_block[i]) + cur_len);
                }
            }
        }
        else{
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * dest->i_block[i]);
            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12]);
            if (getBit(block_bitmap, indirect_block[i-12])){
                while(cur_len < EXT2_BLOCK_SIZE){
                    if((d_p->name_len + sizeof(d_p)) != d_p->rec_len){//this dir_entry_p is the last one
                        int remain_size = d_p->rec_len - sizeof(d_p) - d_p->name_len;
                        if(remain_size > real_size){
                            d_p->rec_len = d_p->name_len + sizeof(d_p);
                            cur_len += d_p->rec_len;
                            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (dest->i_block[i]) + cur_len);
                            memcpy(d_p, source_dp, sizeof(struct ext2_dir_entry_2));
                            strncpy(d_p->name, source_dp->name, source_dp->name_len);
                            d_p->rec_len = EXT2_BLOCK_SIZE - cur_len;
                            return 0;
                        }
                    }
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (dest->i_block[i]) + cur_len);
                }
            }
        }
    }

    //
    i = 0;
    count = 0;
    int cur_len = 0;

    for(;i < (dest->i_blocks/2) + 1; i++){
        if(i<12){
            if(dest->i_block[i]==0){
                empty_block = find_first_empty_block(disk);
                d_p = (struct ext2_dir_entry_2 *)(disk + EXT2_BLOCK_SIZE * empty_block);
                memcpy(d_p, source_dp, sizeof(struct ext2_dir_entry_2));
                strncpy(d_p->name, source_dp->name, source_dp->name_len);
                d_p->rec_len = EXT2_BLOCK_SIZE;
                dest->i_block[i] = empty_block + 1;
                setBit(block_bitmap, empty_block);
                return 0;
            }
        }
        else{
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * source->i_block[i]);
            if(indirect_block[i-12]==0){
                empty_block = find_first_empty_block(disk);
                d_p = (struct ext2_dir_entry_2 *)(disk + EXT2_BLOCK_SIZE * empty_block);
                memcpy(d_p, source_dp, sizeof(struct ext2_dir_entry_2));
                strncpy(d_p->name, source_dp->name, source_dp->name_len);
                d_p->rec_len = EXT2_BLOCK_SIZE;
                indirect_block[i-12] = empty_block + 1;
                setBit(block_bitmap, empty_block);
                return 0;
            }
        }
    }*/
/*
    struct ext2_dir_entry_2* source_dp = NULL;
    //place new dir_entry under destination
    if(!(source_dp = find_dp(disk, argv[2]))){
        printf("Source file not found2\n");
        return ENOENT;
    }
    int real_size = source_dp->name_len + sizeof(source_dp);
    */

