#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
#include "util.c"

unsigned char *disk;

int main(int argc, char **argv) {
    if(argc != 3) {
        fprintf(stderr, "Usage: ext2_cm <image file name> <path>\n");
        exit(1);
    }
    int fd = open(argv[1], O_RDWR);

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
    perror("mmap");
    exit(1);
    }

    struct ext2_super_block *sb;
    struct ext2_group_desc *bg;
    char *block_bitmap;
    char *inode_bitmap;
    struct ext2_inode *inode_table;
    struct ext2_dir_entry_2* d_p = NULL;
    struct ext2_dir_entry_2* new_d_p = NULL;
    unsigned int* indirect_block = NULL;

    find_struct(disk, &sb, &bg, &inode_table, &block_bitmap, &inode_bitmap);
    int inode_num;
    struct ext2_inode *source;

    if(!(source = find_inode_ignore_symlink(disk, argv[2], NULL, &inode_num))){
        printf("File not found\n");
        return ENOENT;
    }
    if(source->i_mode & EXT2_S_IFDIR){
        printf("Can't delete a dir\n");
        return EISDIR;
    }

    int count = 0;
    int i = 0;    

    if(getBit(block_bitmap, source->i_block[12]-1)){
        count ++;
    }

    source->i_links_count --;
    if(source->i_links_count == 0){
        while(count < (source->i_blocks/2)){
            if(i<12){
                d_p = (struct ext2_dir_entry_2 *)(disk + EXT2_BLOCK_SIZE * (source->i_block[i]));
                if (getBit(block_bitmap, source->i_block[i]-1)){
                    count ++;
                    bg->bg_free_blocks_count ++;
                    clearBit(block_bitmap, source->i_block[i]-1);
                }
            }
            else{
                indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * source->i_block[12]);
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12]);
                if (getBit(block_bitmap, indirect_block[i-12]-1)){
                    count ++;
                    bg->bg_free_blocks_count ++;
                    clearBit(block_bitmap, indirect_block[i-12]-1);
                }
            }
            i++;
        }
        clearBit(block_bitmap, source->i_block[12]-1);
        bg->bg_free_blocks_count ++;
        clearBit(inode_bitmap, inode_num - 1);
        bg->bg_free_inodes_count ++;
    }
    
    char *source_name; 
    char *source_dir;

    if(source->i_mode & EXT2_S_IFDIR){
        bg->bg_used_dirs_count --;
    }

    source_dir = split_last_path(argv[2], &source_name);

    if(!source_dir){
        source_dir = "/";
        source_name = argv[2];
    }
    else if (strlen(source_dir) == 0){
        source_dir = "/";
    }
    source = find_inode(disk, source_dir, NULL,0);

    count = 0;
    i = 0;
    int cur_len = 0;
    int prev = 0;
    struct ext2_dir_entry_2 *temp;
    
    if(getBit(block_bitmap, source->i_block[12]-1)){
        count ++;
    }
    while(count < (source->i_blocks/2)){
        if(i<12){
            d_p = (struct ext2_dir_entry_2 *)(disk + EXT2_BLOCK_SIZE * (source->i_block[i]));
            if (getBit(block_bitmap, source->i_block[i]-1)){
                count ++;
                cur_len = 0;
                prev = 0;
                while(cur_len < EXT2_BLOCK_SIZE){
                    if (strlen(source_name) == d_p->name_len){
                        if (!strncmp(source_name, d_p->name,d_p->name_len)){
                            if(d_p->rec_len == EXT2_BLOCK_SIZE){
                                bg->bg_free_blocks_count ++;
                                clearBit(block_bitmap, source->i_block[i]-1);
                                return 0;
                            }
                            else if(prev == cur_len){
                                cur_len += d_p->rec_len;
                                new_d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (source->i_block[i]) + cur_len);
                                temp = malloc(sizeof(struct ext2_dir_entry_2) + sizeof(char) * new_d_p->name_len);
                                memcpy(temp, new_d_p, sizeof(struct ext2_dir_entry_2) + new_d_p->name_len);
                                memcpy(d_p, temp, sizeof(struct ext2_dir_entry_2) + temp->name_len);
                                d_p->rec_len += cur_len;
                                return 0;
                            }
                            else{
                                new_d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (source->i_block[i]) + prev);
                                new_d_p->rec_len += d_p->rec_len;
                                return 0;
                            }
                        }
                    }
                    prev = cur_len;
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (source->i_block[i]) + cur_len);
                }
            }
        }
        else{
            indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * source->i_block[12]);
            d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12]);
            if (getBit(block_bitmap, indirect_block[i-12]-1)){
                count ++;
                cur_len = 0;
                prev = 0;
                while(cur_len < EXT2_BLOCK_SIZE){
                    if (strlen(source_name) == d_p->name_len){
                        if (!strncmp(source_name, d_p->name,d_p->name_len)){
                            if(d_p->rec_len == EXT2_BLOCK_SIZE){
                                bg->bg_free_blocks_count ++;
                                clearBit(block_bitmap, indirect_block[i-12]-1);
                                return 0;
                            }
                            else if(prev == cur_len){
                                cur_len += d_p->rec_len;
                                new_d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12] + cur_len);
                                temp = malloc(sizeof(struct ext2_dir_entry_2) + sizeof(char) * new_d_p->name_len);
                                memcpy(temp, new_d_p, sizeof(struct ext2_dir_entry_2) + new_d_p->name_len);
                                memcpy(d_p, temp, sizeof(struct ext2_dir_entry_2) + temp->name_len);
                                d_p->rec_len += cur_len;
                                return 0;
                            }
                            else{
                                new_d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12] + prev);
                                new_d_p->rec_len += d_p->rec_len;
                                return 0;
                            }
                        }
                    }
                    prev = cur_len;
                    cur_len += d_p->rec_len;
                    d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[i-12] + cur_len);
                }
            }
        }
        i++;
    }
    printf("bug\n");
    return 0;
}

