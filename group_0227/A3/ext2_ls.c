#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
#include "util.c"
unsigned char *disk;


int main(int argc, char **argv) {
    int c;
    int all_flag = 0;
    int fd;
    while ((c = getopt (argc, argv, "a")) != -1){
        switch (c){
            case 'a': all_flag = 1;break;
            default:
                    fprintf(stderr, "Usage: ext2_ls <image file name> <-a> <path>\n");
                    exit(1);
                    break;
        }
    }
    if(argc != 3 && argc != 4) {
        fprintf(stderr, "Usage: ext2_ls <image file name> <-a> <path>\n");
        exit(1);
    }
    if (argc == 3){
        fd = open(argv[1], O_RDWR);
    }
    else{
        fd = open(argv[2], O_RDWR);
    }
     

    disk = mmap(NULL, 128 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(disk == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }
    
    struct ext2_super_block *sb;
    struct ext2_group_desc *gd;
    struct ext2_inode *inode_table;
    char * blockmap;
    char * filename;
    find_struct(disk, &sb,&gd,&inode_table,&blockmap,NULL);
    struct ext2_inode * cur = find_inode(disk,argv[argc-1], &filename,0);
    struct ext2_inode * inodetable = cur;
    int i = 0;
    int cur_len = 0;
    int count = 0;
    struct ext2_dir_entry_2* d_p = NULL;
    unsigned int* indirect_block = NULL;
    int offset = 0;
    //DIRECT NODE
    if (!cur){
        printf("No such file or directory\n");
        return ENOENT;
    }
    if (cur->i_mode & EXT2_S_IFDIR) {
        while(count < (inodetable->i_blocks/2)){
            if (i< 12){
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]));
                cur_len = 0;
                if (getBit(blockmap,inodetable->i_block[i]-1)){
                    count++;
                    while(cur_len < EXT2_BLOCK_SIZE){
                        if(!all_flag){
                            if ((d_p->name_len == 1 && !strncmp(d_p->name,".",1)) || (d_p->name_len == 2 && !strncmp(d_p->name,"..",2))){

                            }
                            else{
                                printf("%.*s\t",d_p->name_len ,d_p->name);
                            }
                        }
                        else{
                            printf("%.*s\t",d_p->name_len ,d_p->name);
                        }
                        cur_len += d_p->rec_len;
                        d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]) + cur_len);
                    }
                }
            } 
            else{
                indirect_block = (unsigned int *)(disk + EXT2_BLOCK_SIZE * inodetable->i_block[i]);
                offset = i - 12;
                d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * indirect_block[offset]);
                cur_len = 0;
                if (getBit(blockmap, indirect_block[offset])){
                    count++;
                    while(cur_len < EXT2_BLOCK_SIZE){
                        printf("%.*s\t",d_p->name_len ,d_p->name);
                        cur_len += d_p->rec_len;
                        d_p = (struct ext2_dir_entry_2* )(disk + EXT2_BLOCK_SIZE * (inodetable->i_block[i]) + cur_len);
                    }
                }
            }
            i++;
        }
        printf("\n");
    }
    else{
        printf("%s\n",filename);
    }





    return 0;
}
